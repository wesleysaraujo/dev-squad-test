<?php

namespace App\Models;

use App\Events\DailyLogCreated;
use Carbon\Carbon;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;
use Illuminate\Database\Eloquent\SoftDeletes;

class DailyLog extends Model
{
    use HasFactory, SoftDeletes;

    /*
     * Fillables fields
     *
     * @var string[]
     */
    protected $fillable = [
        'log',
        'day',
    ];

    /**
     * Attributes must be converted to Carbon instance
     *
     * @var string[]
     */
    protected $dates = [
        'day',
        'deleted_at',
    ];

    /**
     * User relationship
     *
     * @return BelongsTo
     */
    public function user(): BelongsTo
    {
        return $this->belongsTo(User::class);
    }

    /**
     * This query scope filter Daily Logs created from today
     *
     * @param Builder $query
     * @return Builder
     */
    public function scopeFromToday(Builder $query): Builder
    {
        return $query->where('day', '>=', Carbon::today());
    }


}
