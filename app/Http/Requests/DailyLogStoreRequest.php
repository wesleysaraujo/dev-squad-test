<?php

namespace App\Http\Requests;

use App\Rules\BlockShit;
use Illuminate\Foundation\Http\FormRequest;

class DailyLogStoreRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'log' => ['required', new BlockShit],
            'day' => 'required|date',
        ];
    }
}
