<?php

namespace App\Http\Livewire;

use Carbon\Carbon;
use Illuminate\Support\Facades\Validator;
use Livewire\Component;

class Week extends Component
{
    public $days;

    public $currentDate;

    /**
     * Mount the component
     *
     * @return void
     */
    public function mount(): void
    {
        $this->mountWeek(now());
    }

    /**
     * Change Day
     *
     * @param  string $currentDate
     * @return void
     */
    public function changeDay($currentDate): void
    {
        $validatedData = Validator::make(
            ['day' => $currentDate],
            ['day' => 'date'],
        )->validate();

        $currentDate = new Carbon($currentDate);
        $this->mountWeek($currentDate);
        $this->emit('Week.DayChanged');
    }

    /**
     * Assign currentDay the next day
     *
     * @return void
     */
    public function nextDay(): void
    {
        $currentDate = $this->currentDate->addDays(1);
        $this->mountWeek($currentDate, true);
        $this->emit('Week.DayChanged');
    }

    /**
     * Assign currentDay the previous day
     *
     * @return void
     */
    public function previousDay(): void
    {
        $currentDate = $this->currentDate->subDay(1);
        $this->mountWeek($currentDate, true);
        $this->emit('Week.DayChanged');
    }

    /**
     * Render view of component
     *
     * @return \Illuminate\Contracts\View\Factory
     */
    public function render()
    {
        return view('livewire.week');
    }

    /**
     * Private method to populate currentDat and Days attributes
     *
     * @param $currentDate
     * @return void
     */
    private function mountWeek($currentDate): void
    {
        $initDate = $currentDate->copy()->subDays(3);
        $week  = collect();

        for($i = 0; $i <= 6; $i++) {
            $day = $initDate->copy()->addDays($i);
            $week->push($day);
        }

        $this->currentDate = $currentDate;
        $this->days = $week->toArray();
    }
}
