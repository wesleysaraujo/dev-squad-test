<?php

namespace App\Http\Controllers;

use App\Events\DailyLogCreated;
use App\Http\Requests\DailyLogStoreRequest;
use App\Models\DailyLog;
use Illuminate\Http\RedirectResponse;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Event;

class DailyLogController extends Controller
{
    public function store(DailyLogStoreRequest $request)
    {
        $user = auth()->user();

        $input = $request->all();
        $dailyLog = new DailyLog;
        $dailyLog->fill($input);

        $user->dailyLogs()->save($dailyLog);

        Event::dispatch(new DailyLogCreated($dailyLog));
    }

    public function update(Request $request, DailyLog $log): RedirectResponse
    {
        $request->validate([
            'log' => 'required',
        ]);

        $log->log = $request->get('log');
        $log->save();

        return back();
    }

    public function delete(Request $request, DailyLog $log)
    {
        if($request->user()->cannot('delete', $log)) {
            abort(403);
        }

        $log->delete();

        return response()->json($log);
    }

}
