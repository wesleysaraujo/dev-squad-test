<?php

namespace App\Actions\Exam;

use Illuminate\Support\Facades\Http;

class ConsumeAPI
{
    public static function execute()
    {
        $endpoint = config('services.exam.endpoint');
        $authUrl = $endpoint.config('services.exam.auth');
        $meUrl = $endpoint.config('services.exam.me');

        $data = [];

        try {
            $response = Http::asForm()
                ->post($authUrl, [
                    'grant_type' => 'password',
                    'client_id'  => config('services.exam.client_id'),
                    'client_secret' => config('services.exam.client_secret'),
                    'username' => config('services.exam.username'),
                    'password' => config('services.exam.password'),
                    'scope'    =>  '*',
                ]);

            if ($response->successful()) {
                $accessToken = $response->json()['access_token'];

                $response = Http::withHeaders(
                    [
                        'Accept' => 'application/json',
                        'Authorization' => 'Bearer '.$accessToken,
                    ])
                    ->get($meUrl);

                if ($response->successful()) {
                    $data = $response->json();
                }
            }
        } catch (\Exception $e) {
            throw new \RuntimeException($e);
        }

        return $data;
    }
}
