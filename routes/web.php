<?php

use Illuminate\Support\Facades\Route;

Route::view('/', 'tailwind');

Route::prefix('daily-logs')->name('daily-logs.')->group(function() {
    Route::post('/store', 'DailyLogController@store')
        ->name('store')
        ->middleware('block.access');
    Route::put('/update/{log}', 'DailyLogController@update')
        ->name('update');
    Route::delete('/delete/{log}', 'DailyLogController@delete')
        ->name('delete');
});

Route::get('/week', function () {
    return view('components.app');
});
