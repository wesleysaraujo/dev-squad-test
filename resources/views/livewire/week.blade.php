<div>
    <strong>Dia atual: </strong> {{ $currentDate->format('Y-m-d') }}
    <br>
    <br>
    <h3>Dias da semana</h3>
    <br>
    @foreach($days as $day)
        <a href="#" wire:click="changeDay('{{ $day->format('Y-m-d') }}')">{{ $day->format('Y-m-d') }} <br></a>
    @endforeach
    <p class="pt-5">
        <button wire:click="previousDay">Dia Anterior</button>
    </p>
    <p class="pt-5">
        <button wire:click="nextDay">Próximo Dia</button>
    </p>
</div>

{{--
<div class="bg-gray-100 w-1/2 rounded mt5">
    <h1>Visualização da Semana</h1>
    <ul class="p-10">
        @foreach($days as $day)
            <li class="border list-none rounded-sm px-3 py-4 {{ $day['active'] ? 'bg-gray-300' : '' }}">{{ $day['date'] }}</li>
        @endforeach
    </ul>
</div>
--}}
